<?php
	
/*
Plugin Name: BC Flex Content
Plugin URI: http://www.buildcreate.com/
Description: Custom Flex Content Interface based on ACF Pro
Version: 1.3.7
Author: buildcreate, a2rocklobster
Author URI: http://www.buildcreate.com/
Copyright: buildcreate
Bitbucket Plugin URI: http://bitbucket.org/buildcreatestudios/bc-flex-content
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class BC_Flex_Content {

// add styles and scripts to ACF

	
	function __construct() {
		// Enqueue our scripts and styles
		add_action( 'acf/input/admin_enqueue_scripts', array( $this, 'acf_page_builder_enqueue' ));
		add_action('wp_enqueue_scripts', array( $this, 'add_front_styles' ));
		add_action('wp_enqueue_scripts', array( $this, 'front_js' ));
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		add_action( 'admin_init', array( $this, 'settings_init' ) );
		add_action('admin_footer', array( $this, 'remove_acf_fields' ));
		
		// remove flexible content "no content" messge
		add_filter( 'acf/fields/flexible_content/no_value_message', function( $message, $field ){return '';}, 10, 2);
		
		// Hook into content filter to add our layouts
		add_filter('the_content', array($this, 'add_layouts_to_content'));

      // Add GoogleMaps API Key     
		add_filter('acf/fields/google_map/api', function ($api) {
			$api['key'] = 'AIzaSyAE8KM7pt3Px9MQdzoAx_quKShVf5rOCzg';
		   return $api;
		});
 
	}
	
	function acf_page_builder_enqueue(){	
		// acf stylesheet on admin
		wp_enqueue_style( 'acf-page-builder', plugin_dir_url(__FILE__) . '/css/acf.css' ); 
		wp_enqueue_style( 'acf-page-builder-blacktie', plugin_dir_url(__FILE__) . '/css/black-tie.css' ); 		
		// acf page builder js on admin
		wp_enqueue_script( 'acf-page-builder', plugin_dir_url(__FILE__) . '/js/acf-page-builder.js', true );
		//wp_enqueue_script( 'acf-rangeslider', plugin_dir_url(__FILE__) . '/js/nouislider.min.js', true );
	}
	
	function front_js() {
		wp_enqueue_script( 'js-hashchange', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-hashchange/1.3/jquery.ba-hashchange.min.js', true );
		wp_enqueue_script( 'easytabs', plugin_dir_url(__FILE__) . '/js/jquery.easytabs.min.js', true );
		wp_enqueue_script( 'bc-flex-front', plugin_dir_url(__FILE__) . '/js/front.js', true );
	}
	
	function add_front_styles() {
		wp_enqueue_style( 'acf-page-builder-blacktie', plugin_dir_url(__FILE__) . '/css/black-tie.css' );
		wp_enqueue_style( 'acf-page-builder-layouts-dep', plugin_dir_url(__FILE__) . '/css/layouts-dep.css' ); 
		wp_enqueue_style( 'acf-page-builder-layouts', plugin_dir_url(__FILE__) . '/css/layouts.css' ); 
		wp_enqueue_style( 'acf-rangeslider', plugin_dir_url(__FILE__) . '/css/nouislider.css' ); 

		wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAE8KM7pt3Px9MQdzoAx_quKShVf5rOCzg',null,null,true);  
		wp_enqueue_script('googlemaps');		
		
	}
	
	function add_admin_menu(  ) { 
	
		add_submenu_page( 'options-general.php', 'FlexContent', 'FlexContent', 'manage_options', 'flexcontent', array( $this,'options_page' ) );
		
	}
	
	
	function settings_init(  ) { 
	
		register_setting( 'flexcontent', 'flex_settings' );
	
		add_settings_section(
			'bcFlex_section', 
			__( 'Enable Content Types', 'bc-flex' ), 
			array( $this, 'settings_section_callback' ), 
			'flexcontent'
		);
	
		add_settings_field( 
			'bcflex_staff', 
			__( 'Enable Staff', 'bc-flex' ), 
			array( $this,'bcflex_staff_render' ), 
			'flexcontent', 
			'bcFlex_section' 
		);
		
		add_settings_field( 
			'bcflex_resources', 
			__( 'Enable Resources', 'bc-flex' ), 
			array( $this,'bcflex_resources_render' ), 
			'flexcontent', 
			'bcFlex_section' 
		);
		
		add_settings_field( 
			'bcflex_staff_bio_link', 
			__( 'Link to Staff Bios (instead of tooltips)', 'bc-flex' ), 
			array( $this,'bcflex_staff_bio_link' ), 
			'flexcontent', 
			'bcFlex_section' 
		);
	
		add_settings_field( 
			'bcflex_student_group', 
			__( 'Enable Member Groups', 'bc-flex' ), 
			array( $this, 'bcflex_student_group_render' ), 
			'flexcontent', 
			'bcFlex_section' 
		);
	
		add_settings_field( 
			'bcflex_faqs', 
			__( 'Enable FAQ\'s', 'bc-flex' ), 
			array( $this,'bcflex_faqs_render' ), 
			'flexcontent', 
			'bcFlex_section' 
		);
	
	
	}


	function bcflex_resources_render(  ) { 
	
		$options = get_option( 'flex_settings' );
		?>
		<input type='checkbox' name='flex_settings[bcflex_resources]' <?php checked( $options['bcflex_resources'], 1 ); ?> value='1'>
		<?php
	
	}	
	
	function bcflex_staff_render(  ) { 
	
		$options = get_option( 'flex_settings' );
		?>
		<input type='checkbox' name='flex_settings[bcflex_staff]' <?php checked( $options['bcflex_staff'], 1 ); ?> value='1'>
		<?php
	
	}
	
	
	function bcflex_staff_bio_link(  ) { 
	
		$options = get_option( 'flex_settings' );
		?>
		<input type='checkbox' name='flex_settings[bcflex_staff_bio_link]' <?php checked( $options['bcflex_staff_bio_link'], 1 ); ?> value='1'>
		<?php
	
	}
	
	
	function bcflex_student_group_render(  ) { 
	
		$options = get_option( 'flex_settings' );
		?>
		<input type='checkbox' name='flex_settings[bcflex_student_group]' <?php checked( $options['bcflex_student_group'], 1 ); ?> value='1'>
		<?php
	
	}
	
	
	function bcflex_faqs_render(  ) { 
	
		$options = get_option( 'flex_settings' );
		?>
		<input type='checkbox' name='flex_settings[bcflex_faqs]' <?php checked( $options['bcflex_faqs'], 1 ); ?> value='1'>
		<?php
	
	}
	
	
	function settings_section_callback(  ) { 
	
		echo __( 'Manage which content types are active in the CMS.', 'bc-flex' );
	
	}
	
	
	function options_page(  ) { 
	
		?>
		<form action='options.php' method='post'>
	
			<h2>FlexContent</h2>
	
			<?php
			settings_fields( 'flexcontent' );
			do_settings_sections( 'flexcontent' );
			submit_button();
			?>
	
		</form>
		<?php
	
	}
	
	function remove_acf_fields() {
	
		$output = '<script>jQuery(document).ready(function($){$(document).on("click", "a[data-event=\'add-layout\']", function(e) {';
		
		$options = get_option('flex_settings');
		
		if ($options['bcflex_faqs'] != 1) {
			$output .= '$("a[data-layout=\'faqs\']").remove();';
		}
		
		if ($options['bcflex_staff'] != 1) {
			$output .= '$("a[data-layout=\'team_grid\']").remove();';
		}
		
		if ($options['bcflex_resources'] != 1) {
			$output .= '$("a[data-layout=\'resources_grid\']").remove();';
		}
		
		if ($options['bcflex_student_group'] != 1) {
			$output .= '$("a[data-layout=\'student_groups\']").remove();';
		}	
		
		if (!class_exists('Nessie')) {
			$output .= '$("a[data-layout=\'upcoming_events_list\']").remove();';
		}	
		
		if (!class_exists('FundingSerious')) {
			$output .= '$("a[data-layout=\'funding_serious\']").remove();';
		}	
		
		if (!class_exists('CollegiatePoints')) {
			$output .= '$("a[data-layout=\'member_points\']").remove();';
		}
		
		if (!class_exists('Jetpack')) {
			$output .= '$("a[data-layout=\'facebook_widget\']").remove();';
		}		
		
		$output .= 'height = $(".acf-fc-popup").height(); $(".acf-fc-popup").css("margin-top", -height - 40);';
		
		$output .= '}); });</script>';
		
		echo $output;
		
	}
	
	function build_sidebar() {
		if(is_page() && get_field('use_sidebar')) {
			ob_start();
			include(plugin_dir_path(__FILE__).'sidebar.php');
			$sidebar = ob_get_clean();
			
			return $sidebar;
		}

		return false;
		
	}
	
	function add_layouts_to_content($content) {
		if(is_page() && have_rows('rows')) {
			$option = '';
			ob_start();
			include(plugin_dir_path(__FILE__).'layouts.php');
			$layouts = ob_get_clean();
			
			$sidebar = $this->build_sidebar();
			
			if($sidebar) {			
				return $layouts.$sidebar;
			} else {
				return $layouts;
			}

		} else {			
			return $content;
		}
	}
	
	function add_layouts_to_footer() {
		if(have_rows('rows', 'option')) {
			$option = 'option';
			ob_start();
			include(plugin_dir_path(__FILE__).'layouts.php');
			$layouts = ob_get_clean();
			
			echo $layouts;
		}
	}

}



include_once(plugin_dir_path(__FILE__) . 'inc/twitter-widget/wp-latest-twitter-tweets.php');
add_action('acf/init', 'bc_flex_add_field_groups', 999);
add_action('acf/include_field_types', 'bc_flex_content_fields');

function bc_flex_add_field_groups() {
		
	
/* Custom fields for custom post types */
	include_once(plugin_dir_path(__FILE__) . 'defs/custom_fields.php');
	
/* Footer Content Rows */
	include_once(plugin_dir_path(__FILE__) . 'defs/footer-content-field-group.php');
	include_once(plugin_dir_path(__FILE__) . 'defs/footer-widget-fields.php');
	$footer_layouts = array_merge($footer_rows_args['fields'][0]['sub_fields'][9]['layouts'], $footer_fields);
	$footer_rows_args['fields'][0]['sub_fields'][9]['layouts'] = $footer_layouts;
	acf_add_local_field_group($footer_rows_args);

/* Page Content Rows	 */
	include_once(plugin_dir_path(__FILE__) . 'defs/content-rows.php');
	include_once(plugin_dir_path(__FILE__) . 'defs/content-widget-fields.php');
	$content_layouts = array_merge($content_rows_args['fields'][0]['sub_fields'][9]['layouts'], $content_fields);
	$content_rows_args['fields'][0]['sub_fields'][9]['layouts'] = $content_layouts;
	acf_add_local_field_group($content_rows_args);
	
	

/*
	if(current_user_can('administrator')) {
		echo '<pre>';
		print_r($footer_rows_args);
		echo '<pre>';
		die();
	}
*/

	


}

function bc_flex_content_fields() {
	
	// Include our custom fields and gravity form integration	
	include_once(plugin_dir_path(__FILE__) . 'inc/acf-nav-field/nav-menu-v5.php');
	include_once(plugin_dir_path(__FILE__) . 'inc/acf-gravity_forms.php');
	include_once(plugin_dir_path(__FILE__) . 'inc/acf-sidebar_selector-v5.php');
	include_once(plugin_dir_path(__FILE__) . 'inc/acf-widget-field/acf-a_widget.php');

}

if(function_exists('acf_add_options_page')) { 
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title' 	=> 'Site Options',
		'menu_slug' 	=> 'site-options',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
}

// FontAwesome TinyMCE stuff

include(plugin_dir_path(__FILE__).'inc/bfa-lib/better-font-awesome-library.php');


add_action( 'init', 'bc_flex_load_' );

/**
 * Initialize the Better Font Awesome Library.
 *
 * (see usage notes below on proper hook priority)
 */
function bc_flex_load_() {

    // Set the library initialization args (defaults shown).
    $args = array(
            'version'             => 'latest',
            'minified'            => true,
            'remove_existing_fa'  => false,
            'load_styles'         => true,
            'load_admin_styles'   => true,
            'load_shortcode'      => true,
            'load_tinymce_plugin' => true,
    );

    // Initialize the Better Font Awesome Library.
    Better_Font_Awesome_Library::get_instance( $args );
}


// PHP Color manipulation lib
include(plugin_dir_path(__FILE__) . 'inc/phpcolor.php');
$phpcolor = new Mexitek\PHPColors\Color('#fff');				

$options = get_option('flex_settings');

if ($options['bcflex_faqs'] == 1) {
	include(plugin_dir_path(__FILE__) . 'defs/faqs.php');
}

if ($options['bcflex_staff'] == 1) {
	include(plugin_dir_path(__FILE__) . 'defs/staff.php');
	include(plugin_dir_path(__FILE__) . 'defs/staff-types.php');
}

if ($options['bcflex_resources'] == 1) {
	include(plugin_dir_path(__FILE__) . 'defs/resources.php');
}

if (isset($options['bcflex_student_group']) && $options['bcflex_student_group'] == 1) {
	include(plugin_dir_path(__FILE__) . 'defs/student-groups.php');
	include(plugin_dir_path(__FILE__) . 'defs/cohorts.php');
}

$bc_flex_content = new BC_Flex_Content;


?>