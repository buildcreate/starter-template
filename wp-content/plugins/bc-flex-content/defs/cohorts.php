<?php 
	
add_action( 'init', 'cptui_register_my_taxes_cohorts' );

function cptui_register_my_taxes_cohorts() {
	$labels = array(
		"name" => __( 'Cohorts', '' ),
		"singular_name" => __( 'Cohort', '' ),
		);

	$args = array(
		"label" => __( 'Cohorts', '' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Cohorts",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'cohorts', 'with_front' => true ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "cohorts", array( "student_groups" ), $args );

// End cptui_register_my_taxes_cohorts()
}

?>
