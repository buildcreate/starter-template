<?php

add_action( 'init', 'cptui_register_my_cpts_staff' );
function cptui_register_my_cpts_staff() {
	$labels = array(
		"name" => __( 'Staff', '' ),
		"singular_name" => __( 'Staff', '' ),
		);

	$args = array(
		"label" => __( 'Staff', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "staff", "with_front" => true ),
		"query_var" => true,
				
		"supports" => array( "title", "editor", "revisions", "thumbnail", "author" ),				
	);
	register_post_type( "staff", $args );

// End of cptui_register_my_cpts_staff()
}
	
?>