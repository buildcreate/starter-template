<?php 
	
add_action( 'init', 'cptui_register_my_cpts_student_groups' );
function cptui_register_my_cpts_student_groups() {
	$labels = array(
		"name" => __( 'Member Groups', '' ),
		"singular_name" => __( 'Member Group', '' ),
		);

	$args = array(
		"label" => __( 'Member Groups', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "member-group", "with_front" => true ),
		"query_var" => true,
				
		"supports" => array( "title", "editor", "revisions", "thumbnail", "author" ),				
	);
	register_post_type( "student_groups", $args );

// End of cptui_register_my_cpts_student_groups()
}
	
?>