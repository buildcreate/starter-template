jQuery(document).ready(function($){	



	// add modal and show field
	$(document).on('click','.layout .acf-edit-button', function(){
		$('.acf-layout-modal-clickmask').remove();	
		if(!$('.acf-layout-modal-clickmask').length){
			$(this).parent().before('<div class="acf-layout-modal-clickmask"></div>');
			$(this).parent().addClass('open-modal');
			$('.open-modal').prepend('<a href="#" class="acf-flex-modal-close"><span>Done Editing</span>&nbsp; <i class="bts bt-check-circle"> </i></a>');
			$('.open-modal > div').show();
			$('.open-modal .acf-edit-button').hide();
			$('.open-modal .acf-block-width').hide();
			$('.open-modal').animate({
				left: '0'
			});
			$('body').css({'overflow': 'hidden'});
		}
	});
	
	// Ditto but on the close button click
	$(document).on('click', '.acf-flex-modal-close', function() {
		closeAcfModal();
	});	
	
	function closeAcfModal() {
		$('.acf-flex-modal-close').fadeOut();
		$('.acf-flex-modal-close').remove();
		
		$('.open-modal').animate({
			left: '100%'
		}, 300, 'swing', function() {
			
			$('.open-modal > div').hide();
			$('.open-modal .acf-edit-button').show();
			$('.open-modal .acf-block-width').show();			
			$('.open-modal').removeClass('open-modal');		
			$('.acf-layout-modal-clickmask').fadeOut();
			$('body').css({'overflow': 'visible'});	
			
			$('.acf-fields').removeAttr('style');
			
		});			
	}

	// change block width on load
	$('.acf-block-width input').each(function(){
		$(this).parents('.layout').attr('style', 'width:'+ $(this).val() + '%;');
	});

	// change block width on change
	$(document).on('change','.acf-block-width input', function() {
		$(this).parents('.layout').attr('style', 'width:'+ $(this).val() + '%;');
	});

	// add description field to layout handle on load
	function setDescriptions() {
		$('.acf-field-description input').each(function(){
			var desc = $(this).val();
			if(desc){
				$(this).parents('.acf-fields').siblings('.acf-fc-layout-handle').contents().filter(function(){
					return this.nodeType === 3;
				}).remove();
				$(this).parents('.acf-fields').siblings('.acf-fc-layout-handle').append(desc);
			}
		});
	}
	
	setDescriptions();
	
	$(document).on('mouseup', '.ui-sortable-handle', function (){
		setDescriptions();
	});	

	// update description on change
	$(document).on('change', '.acf-field-description input', function(){
		var desc = $(this).val();
		if(desc){
			$(this).parents('.acf-fields').siblings('.acf-fc-layout-handle').contents().filter(function(){
				return this.nodeType === 3;
			}).remove();
			$(this).parents('.acf-fields').siblings('.acf-fc-layout-handle').append(desc);
		}
	});


	
/*
	if($('.opacity-slider')) {
		$('.opacity-slider .acf-input').each(function(){
			
			$(this).prepend('<div class="fc-controller"></div>');
			
			opacitySlider = $('.fc-controller',this)[0];
			
			noUiSlider.create(opacitySlider, {
				start: 80,
				step: 10,
				range: {
					'min' : 0,
					'max' : 100
					}
			});
			
			var inputFormat = document.getElementById($('input', this).attr('id'));

			opacitySlider.noUiSlider.on('update', function( values, handle ) {
				inputFormat.value = values[handle];
			});
			
			inputFormat.addEventListener('change', function(){
				sliderFormat.noUiSlider.set(this.value);
			});
							
		});

	}
*/
		
});