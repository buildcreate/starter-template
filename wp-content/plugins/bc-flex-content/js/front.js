jQuery(document).ready(function($){
	
	// Tabs
	if($('.content-tabs')) {
		$('.content-tabs').easytabs();
		
		function tabResize(){
			$('.tab-nav').each(function() {
				if($(this).height() > 60) {
					$(this).addClass('stacked');
				}
			});

			if($('.stacked')) {
				$('.stacked li a').on('click', function(e) {
					e.preventDefault();
					$(this).parent('li').parent('ul').toggleClass('open');
					
				});
			}			
			
		}
		
		tabResize();
		
		$(window).on('resize', function() {
			tabResize();
		});

		
	}
	
	// Accordions
	
	
	
	
});