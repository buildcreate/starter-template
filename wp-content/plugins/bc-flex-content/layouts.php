<div class="content-wrap <?php if(get_field('use_sidebar') && !$option) {echo 'has-sidebar';} ?>">
<?php 
	if(have_rows('rows', $option)){
		while(have_rows('rows', $option)){
			the_row();

			// get options for this row
			$background_type = get_sub_field('background');
			$background_color = get_sub_field('background_color');
			if($background_type == 'color'){
				$background_style = 'background:'.$background_color. ';';
			}elseif($background_type == 'repeating'){
				$background_image = get_sub_field('background_image');
				$background_style = 'background:url('.$background_image['url']. ');';
			}else{
				$background_image = get_sub_field('background_image');
				$background_style = 'background:url('.$background_image['url']. ') center center;background-size:cover;';
			}
			$max_width = get_sub_field('max_width');
			if($max_width == 0){
				$max_width_style = 'width: auto;';
			}else if($max_width > 100){
				$max_width_style = 'width: 100%;';
			} else {
				$max_width_style = 'width:'.get_sub_field('max_width').'%;';
			}
			
			$mobile_class = get_sub_field('hide_on_mobile') ? 'mobile-hide' : '';
			
			$padding_top = 'padding-top:'.get_sub_field('padding_top').'px;';
			$padding_bottom = 'padding-bottom:'.get_sub_field('padding_bottom').'px;';

			?>
			
			<div class="content-row <?php echo $mobile_class.' '; echo get_sub_field('class').' ';?> 
				
				
					<?php if( isset( $background_image ) ) { echo 'has-image '; } unset($background_image); ?>
						
					<?php global $phpcolor; if($phpcolor->isDark($background_color) == true) { echo 'dark'; } else {echo 'light';} ?>
						
							" style="<?php echo $background_style . $padding_top . $padding_bottom; ?>">
								
				<div class="wrapper" style="<?php echo $max_width_style; ?>">
				<?php 
					// reset block widths
					$block_width_total = 0;

					// get layouts in this row
					if(have_rows('row_layout')){
						while(have_rows('row_layout')){
							the_row();
							
							// get block padding
							$padding_top = get_sub_field('padding_top') ? get_sub_field('padding_top'). 'px' : 0 . 'px';
							$padding_bottom = get_sub_field('padding_bottom') ? get_sub_field('padding_bottom'). 'px' : 0 . 'px';
							$padding_left = get_sub_field('padding_left') ? get_sub_field('padding_left'). 'px' : 0 . 'px';
							$padding_right = get_sub_field('padding_right') ? get_sub_field('padding_right'). 'px' : 0 . 'px';

							// get block width and calculate new rows
							$block_width = get_sub_field('block_width');
							$block_width_total += $block_width;
							if($block_width_total > 100){
								$block_width_total = 0;
								?>
									<span style="clear: both; height: 0;">&nbsp;</span>
									<div class="content-block<?php if($block_width == 100){echo ' full-width-block';}?>" style="float:left; width:<?php echo $block_width; ?>%;padding:<?php echo $padding_top . ' ' . $padding_right . ' ' . $padding_bottom . ' ' . $padding_left; ?>;">
								<?php
							}else{
								?>
									<div class="content-block<?php if($block_width == 100){echo ' full-width-block';}?>" style="float:left;width:<?php echo $block_width; ?>%;padding:<?php echo $padding_top . ' ' . $padding_right . ' ' . $padding_bottom . ' ' . $padding_left; ?>;">
								<?php
							}
							

							
							include(plugin_dir_path(__FILE__).'template-parts/'.get_row_layout().'.php');

							if(!is_home() && !is_category() && !is_archive()) {
								wp_reset_postdata();
							}
							
							?>
								</div>
							<?php
						}
					}
					?>
					<span class="clearer"></span>
				</div>
			</div>
		<?php		
		}
	}
?>
</div>