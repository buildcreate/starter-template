<section class="content-accordion content-block">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	
	<?php 
		$type = get_sub_field('type_of_content'); 
		
		if($type == 'custom_content') {
		if(have_rows('content')): 
		
	?>
	
		<ul class="accordion">
			<?php while(have_rows('content')) : the_row(); $ac_content = get_sub_field('content'); ?>
				<li class="accordion-item">
					<h4 class="accordion-title <?php if($ac_content != '') {echo 'has-content';} ?>"><i class="btr bt-angle-down"></i><?php the_sub_field('title'); ?></h4>
					<div class="user-content accordion-content" style="display:none;"><?php echo $ac_content; ?></div>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
	
	<?php
	
	} else if($type == 'content_type') {
		
		
		$accordion_content = get_sub_field('content_type');  
		
		if($accordion_content) {
		
	?>
	
		<ul class="accordion">
			<?php foreach($accordion_content as $a) : ?>
				<li class="accordion-item">
					<h4 class="accordion-title"><i class="btr bt-angle-down"></i><?php echo $a->post_title; ?></h4>
					<div class="user-content accordion-content" style="display:none;"><?php echo wpautop($a->post_content); ?></div>
				</li>
			<?php endforeach; ?>
		</ul>
		
	<?php
		} 
	}
	?>	
	
</section>