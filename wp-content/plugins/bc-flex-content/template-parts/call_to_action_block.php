<section class="content-call-to-action-block">
	<div class="user-content <?php if(get_sub_field('cta_type') == 'form') {echo 'has-form';} ?>">
		<?php the_sub_field('content'); ?>
	</div>
	<?php if(get_sub_field('cta_type') == 'button') { ?>
		<div class="button-wrap">
			<a class="button" href="<?php echo get_sub_field('button_link'); ?>"><?php echo get_sub_field('button_text'); ?></a>
		</div>
	<?php } else if(get_sub_field('cta_type') == 'form') { ?>
		<?php 
	    $form_object = get_sub_field('form');
	    //gravity_form_enqueue_scripts($form_object['id'], true);
	    gravity_form($form_object['id'], false, true, false, '', true, 1); 
		?>
	<?php } ?>
</section>