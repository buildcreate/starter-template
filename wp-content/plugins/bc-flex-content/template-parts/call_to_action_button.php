<section class="content-call-to-action-button">
	<a class="button" href="<?php echo get_sub_field('button_link'); ?>"><?php echo get_sub_field('button_text'); ?></a>
</section>