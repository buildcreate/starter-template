<section class="content-faqs content-block">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php $faqs = new WP_Query('post_type=faqs&posts_per_page=-1'); ?>
	<?php if($faqs->have_posts()): ?>
		<ul class="accordion faq-list">
			<?php while($faqs->have_posts()) : $faqs->the_post(); ?>
				<li class="accordion-item faq-item">
					<h4 class="accordion-title faq-title"><i class="btr bt-angle-down"></i><?php the_title(); ?></h4>
					<div class="accordion-content faq-content user-content" style="display:none;"><?php the_content(); ?></div>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
</section>