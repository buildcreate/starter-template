<section class="content-wysiwyg">
	<div class="page-content">
		<?php $location = get_sub_field('map');

			if( !empty($location) ) {

		?>
			<div class="acf-map">
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
					<?php echo the_sub_field('location_name') ? the_sub_field('location_name') : ''; ?>
					<?php echo $location['address']; ?>
					<br>
					<a href="https://maps.google.com?q=<?php echo $location['address']; ?>" title="Get directions">Get Directions</a>
				</div>
			</div>
		<?php } ?>
	</div>
</section>

<?php 
	
	include(plugin_dir_path(__FILE__) . '../inc/acf_google_map.php');
	
?>