<?php
	
	$menu_class = get_sub_field('layout');
	if(get_sub_field('show_nested_pages')) {
		$menu_class .= ' show-sub';
	}
	
?>


<section class="content-menu">
		<div class="<?php echo $menu_class; ?>">
			<?php echo get_sub_field('menu'); ?>
		</div>
</section>