<?php
	// get layouts in this nested block
	if(have_rows('nested_layout')){
		?>
		<div class="nested-layout">
		<?php
		while(have_rows('nested_layout')){
			the_row();

			include(plugin_dir_path(__FILE__).'/'.get_row_layout().'.php');

			wp_reset_postdata();
		}
		?>
		</div>
		<?php 
	}
?>