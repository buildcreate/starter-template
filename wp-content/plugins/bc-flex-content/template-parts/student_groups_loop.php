	<?php if($groups->have_posts()) : ?>
		<?php $count = 0; ?>
		<ul class="<?php if(get_sub_field('grid_type') == 'featured') {echo 'grid';} else {echo 'list';} ?>">
		<?php while($groups->have_posts()) : $groups->the_post(); ?>
			<li class="team-member <?php if(get_sub_field('grid_type') == 'featured') {echo grid_class($count, $grid_cols, 1); $count++; } ?>">
				
				<?php if(get_sub_field('grid_type') == 'featured') : ?>
				
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('thumbnail'); ?>
					<?php else : ?>
						<img src="<?php echo plugin_dir_url(__FILE__); ?>../images/default-no-image.jpg" />	
					<?php endif; ?>
					
					
						<h4 class="name"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
						<div class="bio-wrap">
						<div class="website"><?php echo get_field('website'); ?></div>
						<div class="email">
						<?php
							if( have_rows('email_contact') ) {
								
								echo '<strong>Contacts</strong><br>';

							 	// loop through the rows of data
							    while ( have_rows('email_contact') ) : the_row();
							
							        // display a sub field value
							        echo '<a href="'.get_sub_field('email').'">';
							        	the_sub_field('name');
									  echo '</a><br>';
									  
							    endwhile;
							    							
							}
						?>
						</div>
						
						<div class="social">
							<?php if (get_field('facebook') || get_field('twitter')) {?><strong>Social: </strong>&nbsp;<?php } ?>
							<?php if(get_field('facebook')) { echo '<a target="_BLANK" href="'.get_field('facebook').'"><i class="fab fab-facebook"></i></a>'; } ?>&nbsp; 
							<?php if(get_field('twitter')) { echo '<a target="_BLANK" href="'.get_field('twitter').'"><i class="fab fab-twitter"></i></a>'; } ?>
						</div>
						</div>
						<?php if(get_field('facebook') || get_field('twitter') || get_field('email_contact')) { ?><div class="bio-handle">More Info</div><?php } ?>
					
				<?php else : // listing ?>
					
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('thumbnail'); ?>
					<?php else : ?>
						<img src="<?php echo plugin_dir_url(__FILE__); ?>../images/default-no-image.jpg" />	
					<?php endif; ?>
								
						
						<div class="bio-wrap">	
							<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
							<div class="website"><?php echo get_field('website'); ?></div>
							<div class="bio"><?php the_content(); ?></div>
							<div class="email">
							<?php
								if( have_rows('email_contact') ) {
									
									echo '<strong>Contacts</strong><br>';
	
								 	// loop through the rows of data
								    while ( have_rows('email_contact') ) : the_row();
								
								        // display a sub field value
								        echo '<a href="'.get_sub_field('email').'">';
								        	the_sub_field('name');
										  echo '</a><br>';
										  
								    endwhile;
								    							
								}
							?>
						</div>
						
						<div class="social">
							<?php if (get_field('facebook') || get_field('twitter')) {?><strong>Social: </strong>&nbsp;<?php } ?>
							<?php if(get_field('facebook')) { echo '<a target="_BLANK" href="'.get_field('facebook').'"><i class="fab fab-facebook"></i></a>'; } ?>&nbsp; 
							<?php if(get_field('twitter')) { echo '<a target="_BLANK" href="'.get_field('twitter').'"><i class="fab fab-twitter"></i></a>'; } ?>
						</div>
					</div>
				<?php endif; ?>
			</li>
		<?php endwhile; ?>
<!-- 			<li class="clearer"></li> -->
		</ul>
	<?php endif; ?>