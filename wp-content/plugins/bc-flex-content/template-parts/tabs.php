<section class="content-tabs content-block">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	
	
<!-- Tab Nav -->
	
	<?php if(have_rows('tabs')): ?>
	
		<ul class="tab-nav">
			<?php while(have_rows('tabs')) : the_row(); ?>
				<li class="tab">
					<a href="#tab-<?php echo str_replace(' ', '_', preg_replace("/[^a-zA-Z0-9\s]/", "", get_sub_field('tab_title'))); ?>" title="<?php the_sub_field('tab_title'); ?>"><?php the_sub_field('tab_title'); ?></a>
				</li>
				<?php endwhile; ?>
		</ul>
		

<!-- End tab nav	 -->

<!-- Tab Content -->

	<?php while(have_rows('tabs')) : the_row(); ?>
	
		<?php
			
			/* What type of tab content are we dealing with? */
			
			$type = get_sub_field('type_of_content'); 
			
			/* Custom content */
			
			if($type == 'custom_content') {	?>
				
				<div class="tab-content-wrap" id="tab-<?php echo str_replace(' ', '_', preg_replace("/[^a-zA-Z0-9\s]/", "", get_sub_field('tab_title')));; ?>">
						<div class="user-content tab-content"><?php the_sub_field('content'); ?></div>
				</div>	
						
			<?php
				
			} else if($type == 'content_type') {
				
			/*	Post Content */
			
			$tab_content = get_sub_field('select_content');  
			
			if($tab_content) { ?>
		
			<div class="tab-content-wrap" id="tab-<?php echo str_replace(' ', '_', preg_replace("/[^a-zA-Z0-9\s]/", "", get_sub_field('tab_title'))); ?>">
				<?php foreach($tab_content as $a) : ?>
						<h3 class="tab-content-title"><?php echo $a->post_title; ?></h3>
						<div class="user-content tab-content"><?php echo wpautop($a->post_content); ?></div>
				<?php endforeach; ?>
			</div>
			
		<?php
			} 
		}
		?>	
	<?php endwhile; ?>
<?php endif; ?>	
</section>