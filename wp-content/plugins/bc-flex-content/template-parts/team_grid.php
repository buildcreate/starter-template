<section class="content-team-grid">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php $grid_cols = get_sub_field('number_of_grid_columns'); ?>
	<?php $team_roles = get_sub_field('team_roles'); 
			$bcflex_options = get_option('flex_settings');
			
			if($team_roles){ 
					$tax_query = array(
						array(
							'taxonomy' => 'staff_type',
							'field' => 'ids',
							'terms' => $team_roles
						)
					);			
					
				} else {
					$tax_query = array();
				}
		
	?>
	<?php 
		$args = array(
			'post_type' => 'staff',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'tax_query' => $tax_query
		);
		$team = new WP_Query($args);
	?>
	<?php if($team->have_posts()) : ?>
		<?php $count = 0; ?>
		<ul class="<?php if(get_sub_field('grid_type') == 'featured') {echo 'grid';} else {echo 'list';} ?>">
		<?php while($team->have_posts()) : $team->the_post(); ?>
			<li class="team-member <?php if(get_sub_field('grid_type') == 'featured') {echo grid_class($count, $grid_cols, 1); $count++; } ?>">
				
				<?php if(get_sub_field('grid_type') == 'featured') : ?>
				
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('medium'); ?>
					<?php else : ?>
					
					<?php endif; ?>
					
					<div class="bio-wrap">
						<h4 class="name"><?php the_title(); ?></h4>
						<div class="title"><?php echo get_field('title'); ?></div>
						<div class="email"><?php echo get_field('email'); ?></div>
						<?php if(get_the_content()) { ?>
							<div class="bio-handle">
								<?php if($bcflex_options['bcflex_staff_bio_link'] == 1) { echo '<a href="'.get_permalink().'" title="'.get_the_title().'">'; } ?>
								Read Bio
								<?php if($bcflex_options['bcflex_staff_bio_link'] == 1) { echo '</a>'; } ?>
							</div>
						<?php } ?>
						<div class="bio user-content"><h3><?php the_title(); ?></h3><?php the_content(); ?></div>
					</div>
				<?php else : // listing ?>
					
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('thumbnail'); ?>
					<?php else : ?>
					
					<?php endif; ?>
					<div class="bio-wrap">				
						<h3 class="name"><?php the_title(); ?></h3>
						<div class="title"><?php echo get_field('title'); ?></div>
						<div class="email"><?php echo get_field('email'); ?></div>
						<div class="bio user-content"><?php the_content(); ?></div>
					</div>
				<?php endif; ?>
			</li>
		<?php endwhile; ?>
			<li class="clearer"></li>
		</ul>
	<?php endif; ?>
</section>

<?php if($bcflex_options['bcflex_staff_bio_link'] != 1) { ?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>../css/tooltipster.css" />
	<script>
		jQuery(document).ready(function($){
			$('.bio-handle').tooltipster({
				contentAsHTML  : true, 
				maxWidth       : 500,  
				 
				 functionInit: function(){
				 		return $(this).next('.bio').html();
			    },
			    functionReady: function(){
			        $(this).next('.bio').attr('aria-hidden', false);
			    },
			    functionAfter: function(){
			        $(this).next('.bio').attr('aria-hidden', true);
			    }
			});			
		});
	</script>

<?php } ?>