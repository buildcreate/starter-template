<?php 
	
	include(plugin_dir_path(__FILE__).'defs/admin_options.php');
	
class BC_WhiteLabel {
	
	function __construct() {
		add_action( 'admin_enqueue_scripts', array($this, 'load_admin_style') );
		add_action( 'wp_enqueue_scripts', array($this, 'load_front_style'), 999 );
		add_action( 'admin_enqueue_scripts', array($this, 'load_admin_style'), 999 );
		add_action('admin_menu', array($this, 'edit_admin_menus'), 999);
		add_action('init', array($this, 'remove_notices'));
		add_action('admin_footer', array($this, 'admin_header'), 99999);		
		add_filter( 'admin_title', array($this, 'bc_wl_edit_page_title') );
	}
		
	function bc_wl_edit_page_title() {
	    global $post, $title, $action, $current_screen;
	    if($title == 'Widgets') {
	        /* this is the new page title */
	        $title = 'Sidebars';          
	    }
	    return $title;  
	}
	
	function admin_header() {
		?>
		
		<!-- Admin background image -->
		<div id="background-image">
			<?php 			
								
				echo '<img src="'.get_field('background_image', 'option').'" alt="Admin Background" />'; 
				
			?>
		</div>
		<?php
	}
	
	function load_front_style() {
     wp_register_style( 'bc_wl_global', plugin_dir_url( __FILE__ ) . '/css/global.css', false, '1.0.0' );      
     wp_enqueue_style( 'bc_wl_global' );		
	}

	function load_admin_style() {
     wp_register_style( 'bc_wl_global', plugin_dir_url( __FILE__ ) . '/css/global.css', false, '1.0.0' );      
     wp_enqueue_style( 'bc_wl_global' );
     wp_register_style( 'bc_wl', plugin_dir_url( __FILE__ ) . '/css/admin.css', false, '1.0.0' );
     wp_enqueue_style( 'bc_wl' );
     
     wp_register_script( 'bc_wl_admin_js', plugin_dir_url( __FILE__ ) . '/js/admin.js', false, '1.0.0', true);
     wp_register_script( 'bc_wl_background_check', plugin_dir_url( __FILE__ ) . '/js/background-check.js', false, '1.0.0', true);
     wp_enqueue_script( 'bc_wl_admin_js' );
     wp_enqueue_script( 'bc_wl_background_check' );
	}
	
	function remove_notices() {
				 
		 add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
		 add_filter('pre_option_update_core','__return_null');
		 add_filter('pre_site_transient_update_core','__return_null');				
		 add_filter('pre_site_transient_update_plugins','__return_null');
		 add_filter('pre_site_transient_update_core',array($this,'remove_core_updates'));
		 add_filter('pre_site_transient_update_plugins',array($this,'remove_core_updates'));
		 add_filter('pre_site_transient_update_themes',array($this,'remove_core_updates'));
		 add_filter( 'option_wpseo', array($this,'filter_yst_wpseo_options') );
		 add_action( 'wp_loaded', array($this,'suppress_woocommerce_nags'), 99 );
		 
		 remove_action( 'admin_notices', 'woothemes_updater_notice' );
		 remove_action('load-update-core.php','wp_update_plugins');
		 remove_all_actions('admin_notices');
		 
		 add_filter( 'admin_footer_text',    '__return_false', 11 );
		 add_filter( 'update_footer',        '__return_false', 11 );
		 
	}
	
	function filter_yst_wpseo_options( $options ) {
		// Suppress the About page
		$options['seen_about'] = true;
		
		// Suppress the guided tour/feature pointers
		$options['ignore_tour'] = true;
		
		// Return the modified options
		return $options;
	}
	
	function suppress_woocommerce_nags() {
		if ( class_exists( 'WC_Admin_Notices' ) ) {
			// Remove the "you have outdated template files" nag
			WC_Admin_Notices::remove_notice( 'template_files' );
			
			// Remove the "install pages" nag
			WC_Admin_Notices::remove_notice( 'install' );
		}
	}
	
	function remove_core_updates(){
		global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
	}
	
	
	function edit_admin_menus() {
					
			global $menu;
			global $submenu;
/*
			
			echo '<pre>';
				print_r($submenu);
			echo '</pre>';
*/
			
			
			
/*			Define what menu items we want to keep */

			$allowed = array(
				'Dashboard',
				'Posts',
				'Vacancy',
				'Pages',
				'Resources',
				'Media',
				'Forms',
				'Staff',
				'FAQs',
				'Nessie',
				'Member Groups',
				'WooCommerce',
				'Products',
				'Users',
				'Funding Serious',
				'Collegiate Points',
				'Site Options'
			);
			
			
/*		   Get rid of the menu items we don't want   */
			
			foreach($menu as $k => $v) {
				if(!in_array($v[0], $allowed) && !in_array($v[3], $allowed)) {
					unset($menu[$k]);
				}
			}
			
			unset($submenu['index.php']);
			
			$remove = array(
				'wc-addons',
				'wc-status',
			);
			
			foreach($remove as $r) {
				remove_submenu_page( 'woocommerce', $r );
			}
			
/*			Move some submenus to the main nav */

			$new_menus = array(
				array(
					'page_title'    => 'Navigation Menus',
					'menu_title'    => 'Menus',
					'capability'    => 'edit_others_posts',
					'menu_slug'     => 'nav-menus.php',
					'icon_url'      => 'dashicons-list-view',
					'position'      => '70',				
				),
				array(
					
					'page_title'    => 'Sidebars',
					'menu_title'    => 'Sidebars',
					'capability'    => 'edit_others_posts',
					'menu_slug'     => 'widgets.php',
					'icon_url'      => 'dashicons-editor-kitchensink',
					'position'      => '70',
				),
			);
			
			foreach($new_menus as $m) {
				add_submenu_page( 'site-options', $m['page_title'], $m['menu_title'], $m['capability'], $m['menu_slug'] );
			}
					
			return $menu;
			
		}

}
