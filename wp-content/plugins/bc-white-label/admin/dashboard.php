<?php
	
class BC_WL_Dashboard {
	 
	 function __construct() {
		 add_action('admin_head', array($this, 'remove_dashboard_widgets'), 999);
/* 		 add_action('wp_dashboard_setup', array($this, 'dashboard_display'), 999); */
	 }
	 
	 
	 function remove_dashboard_widgets () {

	 	global $wp_meta_boxes;
	 	
	 	$remove = array(
		 	'dashboard_right_now',
		 	'dashboard_activity',
		 	'woocommerce_dashboard_recent_reviews',
	 	);
	 	
	 	foreach ($remove as $r) {
		 	unset($wp_meta_boxes['dashboard']['normal']['core'][$r]);
	 	}
	 	
	 	unset($wp_meta_boxes['dashboard']['side']);
	 	
/*
	 	echo '<pre style="background: #fff;">';
	 	print_r($wp_meta_boxes['dashboard']);
	 	echo '</pre>';
*/
	 	
	 	return;

	}
	
	function dashboard_display() {
		require_once(plugin_dir_path( __FILE__ ).'templates/dashboard-template.php');
		return;
	}
	
	 
}

