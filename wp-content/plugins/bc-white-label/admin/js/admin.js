jQuery(document).ready(function($){ 
	
// Remove screen_options on the dashboard
/*
	$('label[for="dashboard_right_now-hide"]').remove();
	$('label[for="dashboard_activity-hide"]').remove();
	$('label[for="dashboard_quick_press-hide"]').remove();
	$('label[for="dashboard_primary-hide"]').remove();
	$('label[for="wp_welcome_panel-hide"]').remove();
	$('label[for="woocommerce_dashboard_recent_reviews-hide"]').remove();
*/
	
// Change screen_option labels
/*
	input = $('label[for="woocommerce_dashboard_status-hide"] input');
	$('label[for="woocommerce_dashboard_status-hide"]').text('Store Stats');
	$('label[for="woocommerce_dashboard_status-hide"]').prepend(input);
*/

	BackgroundCheck.init({
	  targets: 'h1, .subsubsub, #edit-slug-box, .upload-flash-bypass, .max-upload-size, .drag-drop-inside, .widget-description, .sidebar-description, .seamless',
	  images : '#background-image img',
	});
	
});