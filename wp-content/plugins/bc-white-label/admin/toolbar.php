<?php


class BC_WL_Toolbar {
	
	function __construct() {
		add_action( 'admin_bar_menu', array($this, 'modify_admin_bar'), 999 );
		
		add_action('wp_head', array( $this, 'toolbar_margin' ), 9999);
		
	}
	
	function toolbar_margin() {
		if(is_user_logged_in() && !current_user_can('administrator') && current_user_can('editor')) {
			?> 
			<style>
				html { margin-top: 60px !important; }
				* html body { margin-top: 60px !important; }
				@media screen and ( max-width: 782px ) {
					html { margin-top: 46px !important; }
					* html body { margin-top: 46px !important; }
				}
			</style>
		<?php
		}
	}
	

	function modify_admin_bar( $wp_admin_bar ){
		
/*
			global $submenu;
	echo '<pre style="background: #fff;">';
	print_r($submenu);
	echo '</pre>';
*/
		
		$remove = array(
			'my-blogs',
			'comments',
			'appearance',
			'updates',
			'site-name',
			'wporg',
			'about',
			'documentation',
			'support-forums',
			'feedback',
			'view-site',
			'new-content',
			'customize',
			'itsec_admin_bar_menu',
			'dashboard-link'
		);
		
		foreach($remove as $r) {
			$wp_admin_bar->remove_node($r);
		}
		
		$logo_upload = get_field('admin_bar_logo', 'option');
		
		if($logo_upload) {
			$logo = $logo_upload;
		} else {
			$logo = plugin_dir_url(__FILE__).'images/placeholder-logo.png';
		}
		
		if(!is_admin()) {
			$logo_url = admin_url();
		} else {
			$logo_url = site_url();
		}
		
		$wp_admin_bar->add_node(array(
			'id' => 'wp-logo',
			'title' => '<img id="admin-bar-logo" src="'.$logo.'" alt="Admin" />',
			'href' => $logo_url,
			'meta' => array(
				'class' => 'bc-wl-dash-logo',
			),
		));
		
		$pages_url = 'edit.php?post_type=page';
		$faqs_url = 'edit.php?post_type=faqs';
		$staff_url = 'edit.php?post_type=staff';
		$groups_url = 'edit.php?post_type=student_groups';
		
		if(class_exists('NestedPages')) {
			$pages_url = 'admin.php?page=nestedpages';
			$faqs_url = 'admin.php?page=nestedpages-faqs';
			$staff_url = 'admin.php?page=nestedpages-staff';
			$groups_url = 'admin.php?page=nestedpages-student_groups';
		}
		
		
		$args = array(

			array(
				'id' => 'posts',
				'title' => '<i class="btr bt-align-justify"></i>&nbsp; Posts',
				'href' => 'edit.php',
			),
			array(
				'id' => 'pages',
				'title' => '<i class="btr bt-copy"></i>&nbsp; Pages',
				'href' => $pages_url,
			),
			array(
				'id' => 'forms',
				'title' => '<i class="btr bt-paste"></i>&nbsp; Forms',
				'href' => 'admin.php?page=gf_edit_forms',
			),
			array(
				'id' => 'staff',
				'title' => '<i class="btr bt-user"></i>&nbsp; Staff',
				'href' => $staff_url,
			),
			array(
				'id' => 'member-groups',
				'title' => '<i class="btr bt-user"></i>&nbsp; Groups',
				'href' => $groups_url,
			),
/*
			array(
				'id' => 'product',
				'title' => '<i class="btr bt-shopping-cart"></i>&nbsp; Products',
				'href' => 'edit.php?post_type=product',
			),
*/
			array(
				'id' => 'faqs',
				'title' => '<i class="btr bt-question-circle"></i>&nbsp; FAQ\'s',
				'href' => $faqs_url,
			),
			array(
				'id' => 'post_new',
				'title' => '<i class="btr bt-plus"></i>&nbsp; New Post',
				'href' => 'post-new.php',
				'parent' => 'posts'
			),
			array(
				'id' => 'post_categories',
				'title' => 'Categories',
				'href' => 'edit-tags.php?taxonomy=category',
				'parent' => 'posts'
			),
			array(
				'id' => 'post_tags',
				'title' => 'Tags',
				'href' => 'edit-tags.php?taxonomy=tag',
				'parent' => 'posts'
			),
			array(
				'id' => 'post_order',
				'title' => 'Re-Order Posts',
				'href' => 'edit.php?page=order-post-types-post',
				'parent' => 'posts'
			),
			array(
				'id' => 'page_new',
				'title' => '<i class="btr bt-plus"></i>&nbsp; New Page',
				'href' => 'post-new.php?post_type=page',
				'parent' => 'pages'
			),
			array(
				'id' => 'staff_new',
				'title' => '<i class="btr bt-plus"></i>&nbsp; New Staff',
				'href' => 'post-new.php?post_type=staff',
				'parent' => 'staff'
			),

			array(
				'id' => 'faq_new',
				'title' => '<i class="btr bt-plus"></i>&nbsp; New FAQ',
				'href' => 'post-new.php?post_type=faqs',
				'parent' => 'faqs'
			),

			array(
				'id' => 'groups_new',
				'title' => '<i class="btr bt-plus"></i>&nbsp; New Group',
				'href' => 'post-new.php?post_type=student_groups',
				'parent' => 'member-groups'
			),
			array(
				'id' => 'groups_cohorts',
				'title' => 'Cohorts',
				'href' => 'edit-tags.php?taxonomy=cohorts&post_type=student_groups',
				'parent' => 'member-groups'
			),
			array(
				'id' => 'groups_taxonomy',
				'title' => 'Taxonomy Order',
				'href' => 'edit.php?post_type=student_groups&page=to-interface-student_groups',
				'parent' => 'member-groups'
			),
			array(
				'id' => 'forms_new',
				'title' => '<i class="btr bt-plus"></i>&nbsp; New Form',
				'href' => 'admin.php?page=gf_new_form',
				'parent' => 'forms'
			),
			array(
				'id' => 'forms_entries',
				'title' => 'Entries',
				'href' => 'admin.php?page=gf_entries',
				'parent' => 'forms'
			),
			array(
				'id' => 'forms_export',
				'title' => 'Export/Import',
				'href' => 'admin.php?page=gf_export',
				'parent' => 'forms'
			),
		);
				
		foreach($args as $arg) {		
			$wp_admin_bar->add_node($arg);
		}
		
		
		
/*
		echo '<pre>';
	   	print_r($wp_admin_bar->get_nodes());
	   echo '</pre>';
*/

	}
	
	
}

