<?php
/**
 *
 * @link              http://buildcreate.com
 * @since             1.0.0
 * @package           BC White Label
 *
 * @wordpress-plugin
 * Plugin Name:       BC White Label
 * Plugin URI:        http://buildcreate.com
 * Description:       White labels and restricts user access for customer sites
 * Version:           1.1.3
 * Author:            build/create studios
 * Author URI:        http://buildcreate.com.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bc-wl
 * Domain Path:       /languages
 * Bitbucket Plugin URI: http://bitbucket.org/buildcreatestudios/bc-white-label
 */
 
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
 
 class BC_WHITE_LABEL_INIT {

 	function __construct() {	 	
		require_once(plugin_dir_path( __FILE__ ).'admin/admin.php');
		require_once(plugin_dir_path( __FILE__ ).'admin/dashboard.php');
		require_once(plugin_dir_path( __FILE__ ).'admin/toolbar.php');
	
		add_action('init', 'bc_wl_init');
	
		
	}
	
 }

$bc_wl_init = new BC_WHITE_LABEL_INIT;

function bc_wl_init() {

	// First create our new role and cleanup old roles
	global $wp_roles;
	
   if ( ! isset( $wp_roles ) ) {
      $wp_roles = new WP_Roles();
   }
		
	// Rename roles because it's basically what we want, just plus some stuff
	$wp_roles->roles['editor']['name'] = 'Staff';
	$wp_roles->role_names['editor'] = 'Staff'; 
	
	$wp_roles->roles['subscriber']['name'] = 'Member';
	$wp_roles->role_names['editor'] = 'Member'; 
	
	$wp_roles->roles['contributor']['name'] = 'Member Leader';
	$wp_roles->role_names['contributor'] = 'Member Leader'; 
	
	// Remove roles we don't need
	$rr = array(
		'author',
	);
	
	foreach($rr as $r) {
		remove_role($r);
	}
	
	// Add the other capabilities we need
	$role = get_role( 'editor' );			
	
	$caps = array(
		'edit_dashboard',
		'remove_users',
		'list_users',
		'promote_users',
		'create_users',
		'edit_users',
		'edit_theme_options',
		'edit_posts',
		'gravityforms_edit_forms',
		'gravityforms_delete_forms',
		'gravityforms_create_form',
		'gravityforms_view_entries',
		'gravityforms_edit_entries',
		'gravityforms_delete_entries',
		'gravityforms_view_settings',
		'gravityforms_edit_settings',
		'gravityforms_export_entries',
		'gravityforms_import_entries',
		'gravityforms_view_entry_notes',
		'gravityforms_edit_entry_notes',
		'gravityforms_preview_forms',
		'manage_woocommerce',
		'view_woocommerce_reports',
		'manage_options',
		'delete_users',
		'assign_product_terms',
		'assign_shop_coupon_terms',
		'assign_order_terms',
		'delete_others_products',
		'delete_others_shop_orders',
		'delete_private_products',
		'delete_shop_coupons',
		'delete_shop_orders',
		'delete_product',
		'delete_product_terms',
		'delete_products',
		'delete_published_products',
		'delete_published_shop_coupons',
		'delete_published_shop_orders',
		'delete_shop_coupon',
		'delete_shop_coupon_terms',
		'delete_shop_coupons',
		'delete_shop_orders',
		'delete_shop_order_terms',
		'edit_others_products',
		'edit_others_shop_coupons',
		'edit_others_products',
		'edit_others_shop_orders',
		'edit_private_products',
		'edit_private_shop_coupons',
		'edit_private_shop_orders',
		'edit_product',
		'edit_product_terms',
		'edit_products',
		'edit_published_products',
		'edit_published_shop_coupons',
		'edit_published_shop_orders',
		'edit_shop_coupon',
		'edit_shop_coupon_terms',
		'edit_shop_coupons',
		'edit_shop_order',
		'edit_shop_order_terms',
		'edit_shop_orders',
		'gravityforms_authorizenet',
		'gravityforms_user_registration',
		'manage_product_terms',
		'manage_shop_coupon_terms',
		'manage_shop_order_terms',
		'publish_products',
		'publish_shop_coupons',
		'publish_shop_orders',
		'read_private_products',
		'read_private_shop_coupons',
		'read_private_shop_orders',
		'read_product',
		'read_shop_coupon',
		'read_shop_order',
				
	);
	
	if($role) {
		foreach($caps as $c) {
			$role->add_cap( $c );
		}
	}
	
	if(!current_user_can('administrator')) {	
		$BCWhiteLabel = new BC_WhiteLabel;
		$bcwldash = new BC_WL_Dashboard;
		$bc_wl_toolbar = new BC_WL_Toolbar;
	}
}

?>