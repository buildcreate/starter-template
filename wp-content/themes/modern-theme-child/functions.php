<?php
		
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function child_header_styles() {	
	
	global $mo_options;
	
?>
	
	<style>
		.content-row.light .button {
			background: <?php echo $mo_options->theme_options['primary_dark']; ?> !important;
			color: #fff !important;
		}
		
		.dark .button {
			background: #fff;
			color: <?php echo $mo_options->theme_options['primary_dark']; ?>;
		}		
		
		#header-hero input[type="submit"].button {
			background: #fff !important;
			color: <?php echo $mo_options->theme_options['primary_dark']; ?> !important;
			font-size: 1.2em;
		}		
		
		.dark .content-block .button:visited {
			color: <?php echo $mo_options->theme_options['primary_dark']; ?> !important;
		}
		
	</style>
	
	<link rel="Shortcut Icon" type="image/x-icon" href="/wp-content/themes/modern-org-child/favicon.png">
	
<?php
}

add_action('wp_head', 'child_header_styles', 9999);

?>