		<!-- footer -->
		<?php global $mo_options; global $phpcolor; ?>
		
		<footer id="footer" class="<?php if($phpcolor->isDark($mo_options->theme_options['footer_background_color'])) {echo 'dark';} ?>" role="contentinfo">
			
			<?php global $bc_flex_content; $bc_flex_content->add_layouts_to_footer(); ?>
						
			<div class="wrapper">
				
				<?php if(get_field('add_to_footer', 'options')) { ?>
					<div class="footer-social">
						Connect with us
						<ul>
							<?php 
								echo mo_social_icons('footer');
							?>
						</ul>
					</div>
				<?php } ?>
						
				<div class="copyright">
					&copy; <?php echo get_field('name', 'option'); ?> <?php echo date('Y', time()); ?> <span style="float: right;"><em><a target="_BLANK" href="http://buildcreate.com" title="Michigan Web Design">Michigan Web Design</a> by <a target="_BLANK" href="http://buildcreate.com" title="Michigan Web Design">build/create</a></em></span>
				</div>
			</div>
		</footer>
		<!-- /footer -->
	
		</div><!-- /.bg-overlay -->
		<?php wp_footer(); ?>
	</body>
</html>