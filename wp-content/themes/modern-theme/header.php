<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		
		<!-- dns prefetch -->
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		
		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		
		<!-- icons -->
		<link href="<?php echo CHILD_DIR; ?>/favicon.png" rel="shortcut icon">
		
		<!-- css + javascript -->
		<?php wp_head(); ?>
		
		<?php if(is_user_logged_in() && !current_user_can('administrator')) { ?>
			<script>
				jQuery(document).ready(function($) {
					if($('#wpadminbar') && $('#header').hasClass('sticky')) {
						adminBarHeight = $('#wpadminbar').height();
						$('#header').css('top',adminBarHeight+'px');
					}
				});
			</script>
		<?php } ?>
		
</head>
	<body <?php body_class(); ?>>
		<div class="bg-overlay">
		<!-- skip to content for screen readers -->
		<a class="hidden" href="#main">Skip to content</a>
		<!-- back to top -->
		<div id="top-link"><i class="btl bt-angle-up"></i><span>TOP</span></div>
		<!-- header -->
		
		<header id="header" role="banner" class="<?php echo modern_header_class(); ?>">
			<div class="drawer">
				<ul class="utility-nav">
					<div class="wrapper">						
						<div class="left">
							<ul>
							<?php do_action('mo_before_left_utility'); ?>
							<?php echo mo_header_contact(); ?>
							<?php do_action('mo_after_left_utility'); ?>
							</ul>
						</div>
						
						<div class="right">
							<?php do_action('mo_before_right_utility'); ?>
							<li class="header-search"><?php get_search_form(); ?></li>
							<?php if(is_user_logged_in()) : ?>
								<li><a href="<?php echo wp_logout_url(home_url()); ?>" title="login">Logout</a></li>
							<?php else : ?>
								<li><a href="/get-involved/" title="register">Register</a></li>
								<li class="pipe"><a href="<?php echo wp_login_url(); ?>" title="login">Login</a></li>
							<?php endif; ?>
							<?php if(class_exists('woocommerce')) { ?>
							<li class="pipe"><a href="/my-account/" title="profile">Edit profile</a></li>
							<li class="cart pipe">
								<a href="<?php bloginfo('url'); ?>/cart" title="View Cart">
									(<?php global $woocommerce; echo $woocommerce->cart->cart_contents_count; ?>) Cart <i class="bts bt-shopping-cart"></i>
								</a>
							</li>
							<?php } ?>
							<?php do_action('mo_after_right_utility'); ?>
						</div>	
					</div>
				</ul>
			</div>
			<div id="drawer-handle">Contact & Login <i class="btr bt-angle-down"></i></div>
			
			<!-- wrapper -->
			<div class="wrapper">
				<!-- logo -->
				<div class="logo">
					<?php global $mo_options; if(is_page('home')) : ?>
						<?php if($mo_options->theme_options['logo']) : ?>
							<img src="<?php echo $mo_options->theme_options['logo']; ?>" alt="Logo" class="logo-img">
						<?php else : ?>
							<img src="<?php echo bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" class="logo-img" />
						<?php endif; ?>
					<?php else : ?>
						<a href="<?php echo home_url(); ?>">
							<?php if($mo_options->theme_options['logo']) : ?>
								<img src="<?php echo $mo_options->theme_options['logo']; ?>" alt="Logo" class="logo-img">
							<?php else : ?>
								<img src="<?php echo bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" class="logo-img" />
							<?php endif; ?>
						</a>
					<?php endif; ?>
				</div>

				<!-- /logo -->


				<?php // mobile nav handle ?>
				<a class="nav-handle" href="#mobile-nav"><i class="btr bt-bars"></i></a>

				<!-- main nav -->
				<nav class="main-nav" role="navigation">
					<ul> 
						<?php wp_nav_menu(array('theme_location' => 'header', 'items_wrap' => '%3$s', 'container'=> false)); ?>
						<?php do_action('mo_inside_nav'); ?>
					</ul> 
				</nav>
				<!-- /nav -->
				<span class="clearer"></span>
			</div>
			<?php do_action('mo_after_nav'); ?>
		</header>
		<!-- /header -->
			<?php do_action('mo_after_header'); ?>