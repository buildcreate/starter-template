(function() {
    tinymce.create("tinymce.plugins.modern_button_plugin", {

        //url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            //add new button     
            ed.addButton("modern_button", {
                title : "Add Button",
                text : "Add Button",
                icon : false,
                cmd : "modern_button_command",
            });

            //button functionality.
            ed.addCommand("modern_button_command", function() {
                var selected_text = ed.selection.getContent();
                var return_text = "<a class='button' title=" + selected_text + ">" + selected_text + "</a>";
                ed.execCommand("mceInsertContent", 0, return_text);
            });

        },

    });

    tinymce.PluginManager.add("modern_button_plugin", tinymce.plugins.modern_button_plugin);
})();