<?php
	
	// Header display login
	
	
	
	function mo_header_logic($mo_options) {
		
		$mo_options->init_post_options();
				
		if($mo_options->page_options['header_type'] == 'slider') {
			
			if( have_rows('slides') ):
					
				echo '<div id="header-slider">';
				 	// loop through the rows of data
				    while ( have_rows('slides') ) : the_row();
					 		
					 		$title = get_sub_field('title');
					 		$content = get_sub_field('content');
					 		$url = get_sub_field('url');
					 		$btn_text = get_sub_field('button_text');
					 		$overlay = get_sub_field('color_overlay');
					 		$opacity = get_sub_field('overlay_opacity');
					 		$unboxed = get_sub_field('unbox_slide_content');
					 		$mobile_class = get_sub_field('hide_content_on_mobile') ? 'mobile-hide' : '';
					 		
					 		if($unboxed == true) {$unboxed = 'unboxed';} else {$unboxed = '';}
		 					$content_position = explode('-', get_sub_field('content_position'));
			
							if($content_position[0] == 'middle') {
								$content_position[] = 'vertical-center';
							}
							
							$content_position = implode(' ', $content_position);
							
							$header_height = $mo_options->page_options['header_height'];
							
							switch($header_height) {
								case '25vh':
									$header_min = '300px;';
									$hclass = 'short';
								break;
								case '50vh':
									$header_min = '388px;';
									$hclass = 'medium';
								break;
								case '70vh':
									$header_min = '450px;';
									$hclass = 'tall';
								break;
								case '100vh':
									$header_min = '600px;';
									$hclass = 'full';
								break;
							}
							
							
							
							if($mo_options->theme_options['transparent_navigation_bar']) {
								
								switch($header_height) {
									case '25vh':
										$header_height = '50vh';
										$header_min = '400px;';
										$hclass = 'short';
									break;
									case '50vh':
										$header_height = '70vh';
										$header_min = '488px;';
										$hclass = 'medium';
									break;
									case '70vh':
										$header_height = '85vh';
										$header_min = '550px;';
										$hclass = 'tall';
									break;
									case '100vh':
										$header_min = '700px;';
										$hclass = 'full';
									break;
								}
								
							}
							
							$header_height = apply_filters('mo_slider_header_height', $header_height, $header_min, $hclass);
							$header_min = apply_filters('mo_slider_header_height_min', $header_min, $header_height, $hclass);
						
							$header_height_style = 'height: '.$header_height.'; min-height: ';
							$header_height_style .= $header_min;
													
							if($overlay) {$overlay_style = 'background: rgba('.hex_to_rgb($overlay).', '. $opacity/100 .');';}		 		
					 	
					 		echo '<div class="'.$hclass.'" style="'.$header_height_style.' background-image: url('.get_sub_field('image').'); background-size: cover;">';
					 		
					 		if($overlay) { echo '<div id="header-overlay" style="'.$overlay_style.'">'; }
					 			
					 			if($title || $content || $btn_text) {
						 			echo '<div class="slide-content '.$hclass.' '.light_or_dark($overlay).' '.$content_position.' '.$unboxed.'" style="width:'.get_sub_field('slide_width').'%;">';
						 			if($title && $content) {echo '<div class="slide-body page-content">';}
						 			if($title) { echo '<h2>'.$title.'</h2>'; }
						 			if($content) { echo '<div class="'.$mobile_class.'">'.$content.'</div>'; }
						 			if($title && $content) {echo '</div>';}
						 			if($btn_text) { echo '<div class="slide-footer"><a class="button '.$mo_options->theme_options['button_style'].'" href="'.$url.'" title="'.$title.'">'.$btn_text.'</a></div>'; }
						 			echo '</div>';
					 			}
					 		echo '</div>';
					 	
					 	if($overlay) {	echo '</div>'; }
					 		
				    endwhile;
				 echo '</div>';
			else :
			
			    // no rows found
			
			endif;
			
			
		} 
		
		if($mo_options->page_options['header_type'] == 'static') {
			
			$header_background = $mo_options->page_options['header_background'];
			$header_height = $mo_options->page_options['header_height'];
			$overlay = $mo_options->page_options['color_overlay'];
			$opacity = $mo_options->page_options['overlay_opacity'];
			$title = $mo_options->page_options['header_title'];
			$content = $mo_options->page_options['header_content'];
			$include_cta = $mo_options->page_options['include_cta'];
			$button_text = $mo_options->page_options['button_text'];
			$button_url = $mo_options->page_options['button_url'];
			$content_position = $mo_options->page_options['content_position'];
			$content_width = $mo_options->page_options['content_width'];
			$mobile_class = $mo_options->page_options['hide_content_on_mobile'] ? 'mobile-hide' : '';
			
			$content_position = explode('-', $content_position);
			
			if($content_position[0] == 'middle') {
				$content_position[] = 'vertical-center';
			}
			
			$content_position = implode(' ', $content_position);
			
			switch($header_height) {
				case '25vh':
					$header_min = '300px;';
					$hclass = 'short';
				break;
				case '50vh':
					$header_min = '388px;';
					$hclass = 'medium';
				break;
				case '70vh':
					$header_min = '450px;';
					$hclass = 'tall';
				break;
				case '100vh':
					$header_min = '600px;';
					$hclass = 'full';
				break;
			}
			
			if($mo_options->theme_options['transparent_navigation_bar']) {
				
				switch($header_height) {
					case '25vh':
						$header_height = '35vh';
						$header_min = '400px;';
					break;
					case '50vh':
						$header_height = '60vh';
						$header_min = '488px;';
					break;
					case '70vh':
						$header_height = '70vh';
						$header_min = '550px;';
					break;
					case '100vh':
						$header_min = '700px;';
					break;
				}
				
			}
	
			$header_height = apply_filters('mo_static_header_height', $header_height, $header_min, $hclass);
			$header_min = apply_filters('mo_static_header_height_min', $header_min, $header_height, $hclass);
		
			$header_height_style = 'height: '.$header_height.'; min-height: ';
			$header_height_style .= $header_min;
			
			if($overlay) {$overlay_style = 'background: rgba('.hex_to_rgb($overlay).', '. $opacity/100 .');';}
			
			echo '<div class="'.$hclass.'" id="header-hero" style="'.$header_height_style.'">';
				if($overlay) { echo '<div id="header-overlay" style="'.$overlay_style.'">'; }
					if($title || $content || $include_cta == 'yes') {
						
						echo '<div class="hero-content '.$hclass.' '.light_or_dark($overlay).' unboxed '.$content_position.' dark" style="width: '.$content_width.'%;">';
							
							if($title || $content) { echo '<div class="hero-content-body">'; }
								if($title) { echo '<h1>'.$title.'</h1>'; }
								if($content) { echo '<div class="'.$mobile_class.'">'.$content.'</div>'; }
							if($title || $content) { echo '</div>'; }
							
							if($include_cta == 'yes') { 
								echo '<div class="hero-content-footer">'; 
									echo '<a class="button '.$mo_options->theme_options['button_style'].'" href="'.$button_url.'">'.$button_text.'</a>';
								echo '</div>';							
							}								
							
						echo '</div>';
						
					}
				if($overlay) {	echo '</div>'; }
				echo '<img id="header-background-image" src="'.$header_background.'" alt="'.$title.'" />';
			echo '</div>';
			
		}

		if($mo_options->page_options['header_type'] == 'default' || !$mo_options->page_options['header_type']) {
			
			$header_style = 'height: 25vh; min-height: 200px;';
			
			if($mo_options->theme_options['transparent_navigation_bar']) {
				$header_style = 'height: 30vh; min-height: 300px;';
				$content_style = 'transform: none;';
			}
			
			if($mo_options->page_options['background_image']) {
				$header_bg = $mo_options->page_options['background_image'];
			} else {
				$header_bg = $mo_options->theme_options['default_page_header_background'];
			}
			
			if($mo_options->page_options['title_alignment'] == '') {
				$mo_options->page_options['title_alignment'] = 'left';
			}
			
			$header_style = apply_filters('mo_default_header_style', $header_style);
			
			?>
		
			<div class="short default" id="header-hero" style="<?php echo $header_style; ?>">
				<div id="header-overlay" style="background: rgba(0 ,0 ,0, 0.7);">
					<div class="wrapper" style="max-width:<?php echo $mo_options->theme_options['site_max_width']; ?>;">
						<div class="hero-content short dark-overlay unboxed middle left vertical-center dark" style="<?php echo $content_style; ?>">
							<div class="hero-content-body">
								<h1 style="text-align: <?php echo $mo_options->page_options['title_alignment']; ?>;">
									
									<?php 
										
									if(is_single() || is_page()) {the_title();} 
									
									if(is_category() || is_tax() || is_tax()) {
										echo '<small>';
											single_term_title('Browsing: ');
										echo '</small>';
									}
									
									if(is_search()) {
										echo '<small>Search results for: "'.$_GET['s'].'"</small>';
									}
									
									if(is_post_type_archive('product')) {echo apply_filters('mo_shop_title', 'Blog');} 
									
									if(is_home()) {echo apply_filters('mo_blog_title', 'Blog');} 
									
									if(is_singular('staff')) {
										echo '<small style="font-size: 25px; display: block; opacity: .8">'.get_field('title').'</small>';
									}
									
									?>
									
									
									</h1>
							</div>
						</div>
					</div>
				</div>
				<img id="header-background-image" src="<?php echo $header_bg; ?>" alt="<?php the_title(); ?>">
			</div>			
			
			<?php
			
		}

		
	}
	
	add_action('mo_render_header', 'mo_header_logic');
	
	function mo_header_contact() {
		
		global $mo_options;
		
		if($mo_options->site_options['contact_phone']) {
			echo '<li class="header-phone">';
				echo '<i class="bts bt-phone"></i> '.$mo_options->site_options['contact_phone'];
			echo '</li>';
		}

		if($mo_options->site_options['contact_email']) {
			echo '<li class="header-email">';
				echo '<a href="mailto:'.$mo_options->site_options['contact_email'].'"><i class="bts bt-envelope"></i> '.$mo_options->site_options['contact_email'].'</a>';
			echo '</li>';
		}
		
		mo_social_icons('header');
				
	}
	
	function mo_social_icons($location = 'header') {
		
		global $mo_options;
		$output = array();
		
		if($mo_options->site_options['social_icons']) {
			foreach($mo_options->site_options['social_icons'] as $s) {
				$output[] = '<li class="social-icon"><a class="footer-social-icon" target="_BLANK" href="'.$s['url'].'"><i class="fab fab-'.$s['icon'].'"></i></a></li>';
			}
		}
		
		$output = apply_filters('mo_social_icons', $output, $location);
		
		foreach($output as $o) {
			echo $o;
		}
		
	}