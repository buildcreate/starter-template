<?php 
	
		// add page content wrapper and class to the_content
	function wrap_the_content($content){
		$content = '<div class="user-content">' . $content . '<span class="clearer"></span></div>';
		return $content;
	}
	add_filter('the_content','wrap_the_content',11);
	

	// read more for blog posts
	function new_excerpt_more($more) {
		global $post;
		return '... <a class="read-more" href="'. get_permalink($post->ID) . '">read more</a>';
	}
	
	add_filter('excerpt_more', 'new_excerpt_more');
	
	function modern_header_class() {
		global $mo_options;
		
		$class = '';
		
		if($mo_options->theme_options['sticky_header'] == true) {$class .= ' sticky';}
		
		if($mo_options->theme_options['transparent_navigation_bar'] == true) {$class .= ' transparent';}
		
		if($mo_options->theme_options['increase_navigation_contrast'] == true) {$class .= ' darken';}
		
		return $class;
		
	}
	
	function light_or_dark($hex) {
	   $hex       = str_replace('#', '', $hex);
	   $r         = (hexdec(substr($hex, 0, 2)) / 255);
	   $g         = (hexdec(substr($hex, 2, 2)) / 255);
	   $b         = (hexdec(substr($hex, 4, 2)) / 255);
	   $lightness = round((((max($r, $g, $b) + min($r, $g, $b)) / 2) * 100));
	   
	   if($lightness >= 50) {
		   return 'light-overlay';
	   } else {
		 	return 'dark-overlay';  
	   }
	}
	
	// convert hex to rgb
	function hex_to_rgb($hex){
		$hex = str_replace("#", "", $hex);
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
		return $r.' ,'.$g.' ,'.$b;
	}

	function adjust_brightness($hex, $percent, $darken = false) {

	    $brightness = $darken ? -255 : 255; 
	    
	    $steps = $percent*$brightness/100;
	
	    // Normalize into a six character long hex string
	    $hex = str_replace('#', '', $hex);
	    if (strlen($hex) == 3) {
	        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
	    }
	
	    // Split into three parts: R, G and B
	    $color_parts = str_split($hex, 2);
	    $return = '#';
	
	    foreach ($color_parts as $color) {
	        $color   = hexdec($color); // Convert to decimal
	        $color   = max(0,min(255,$color + $steps)); // Adjust color
	        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
	    }
	
	    return $return;
	}

		
	// empty search fix
	function my_request_filter( $query_vars ) {
		if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
			$query_vars['s'] = " ";
		}
		return $query_vars;
	}
	add_filter('request', 'my_request_filter');

	// custom excerpt length function, parameters are content to be truncated, length desired, and desired "read more" text
	function custom_excerpt($length = 150, $content = null, $linktext = null) {
		
		global $post;
		
		if(!$content){
			$content = $post->post_content;
		}
		
		$content = strip_tags($content);

   	if(strlen($content) > $length) {
       	while($content{$length} !== ' ') {
          	$length--;  
       	}
       	$content = substr($content, 0, $length); 
   	}
   	if(!$linktext){
   		$linktext = "Read more";
   	}
   	
   	return $content . '... <a class="read-more" href="'.get_permalink($post->ID).'"> '. $linktext . '</a>'; 
	   	
	} 

	// Assign current template to global
	
	function var_template_include($t){
	   $GLOBALS['current_theme_template'] = basename($t);
	   return $t;
	}
	
	add_filter('template_include', 'var_template_include', 1000);
	
	// to print: get_current_template(true);
	
	function get_current_template($echo = false){
		
		if(!isset($GLOBALS['current_theme_template'])){
			return false;
		}
		if( $echo ){
			echo $GLOBALS['current_theme_template'];
		} else {
			return $GLOBALS['current_theme_template'];
		}  
		
	}  

	// Add page slug to body class
	function add_slug_to_body_class($classes){
		
	    global $post;
	    
		if (is_home()) {
		    
			$key = array_search('blog', $classes);
			if ($key > -1) {
			   unset($classes[$key]);
			}
	        
	    } elseif (is_page()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    } elseif (is_singular()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    }
	    
	    return $classes;
	    
	}
	            
	add_filter('body_class', 'add_slug_to_body_class');

	
	// responsive grid system class generator function
	function grid_class($count, $num_cols = 4, $min_cols = 2){
		
		switch ($min_cols) {
			case 2 : $min = 'two-col-min'; break;
			case 3 : $min = 'three-col-min'; break;
			case 4 : $min = 'four-col-min'; break;
			default: $min = 'no-min'; break;
		}

		if($num_cols == 7) {
	       $klass = 'seven-col-grid '. $min;
	       if($count%2 == 0){$klass .= ' two-col-clear';}
	       if($count%3 == 0){$klass .= ' three-col-clear';}
	       if($count%4 == 0){$klass .= ' four-col-clear';} 
	       if($count%5 == 0){$klass .= ' five-col-clear';}
	       if($count%6 == 0){$klass .= ' six-col-clear';} 
	       if($count%7 == 0){$klass .= ' seven-col-clear';}
	    }else if($num_cols == 6) {
	       $klass = 'six-col-grid '. $min;
	       if($count%2 == 0){$klass .= ' two-col-clear';}
	       if($count%3 == 0){$klass .= ' three-col-clear';}
	       if($count%4 == 0){$klass .= ' four-col-clear';} 
	       if($count%5 == 0){$klass .= ' five-col-clear';}
	       if($count%6 == 0){$klass .= ' six-col-clear';}
	    }else if($num_cols == 5) {
	       $klass = 'five-col-grid '. $min;
	       if($count%2 == 0){$klass .= ' two-col-clear';}
	       if($count%3 == 0){$klass .= ' three-col-clear';}
	       if($count%4 == 0){$klass .= ' four-col-clear';} 
	       if($count%5 == 0){$klass .= ' five-col-clear';}
	    }else if($num_cols == 4) {
	       $klass = 'four-col-grid '. $min; 
	       if($count%2 == 0){$klass .= ' two-col-clear';}
	       if($count%3 == 0){$klass .= ' three-col-clear';}
	       if($count%4 == 0){$klass .= ' four-col-clear';} 
	    }elseif($num_cols == 3){
	       $klass = 'three-col-grid '. $min;
	       if($count%2 == 0){$klass .= ' two-col-clear';}
	       if($count%3 == 0){$klass .= ' three-col-clear';}
	    }elseif($num_cols == 2){
	       $klass = 'two-col-grid '. $min; 
	       if($count%2 == 0){$klass .= ' two-col-clear';}
	    }
	    return $klass;  
	}
	
?>