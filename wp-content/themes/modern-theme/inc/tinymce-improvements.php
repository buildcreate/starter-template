<?php
	///////////////////////////////////////////////
	//---------- TINY MCE IMPROVEMENTS --------////
	///////////////////////////////////////////////
	
	
	add_filter('tiny_mce_before_init', 'dynamic_editor_styles', 10);
	
	function dynamic_editor_styles($settings){
	    // you could use a custom php file as well, I'm using wp_ajax as
	    // callback handler for demonstration
	    // content_css is a string with several files seperated by a comma
	    // e.g. file1, file2, ... extend the string
	
	    $settings['content_css'] .= ",".admin_url('admin-ajax.php') ."/?action=dynamic_styles";
	
	    return $settings;
	}
	
	// add wp_ajax callback
	add_action('wp_ajax_dynamic_styles', 'dynamic_styles_callback');
	function dynamic_styles_callback(){
	    global $mo_options;	    
	    header("Content-type: text/css; charset: UTF-8");
	    ?>
	    
			body, html, h1, h2, h3, h4, h5, h6, a, p, blockquote, ul, li, ol, button, input, table, th, td, tr {
				font-family: <?php echo $mo_options->theme_options['font']['font']; ?>
			}
			
			a {
				color: <?php echo $mo_options->theme_options['primary_dark']; ?>;
			}
			
			a:hover {
				color: <?php echo adjust_brightness($mo_options->theme_options['primary_dark'], 20); ?>;
			}
			
			h1 {color: <?php echo $mo_options->theme_options['primary_dark']; ?>;}	    	    
			h2 {color: <?php echo $mo_options->theme_options['secondary_dark']; ?>;}
			.button, .button.light {color: <?php echo $mo_options->theme_options['primary_dark']; ?>;}

			<?php switch($mo_options->theme_options['button_style']) { case 'light': ?>
					.button {
						color: <?php echo $mo_options->theme_options['primary_dark']; ?>; 
						border-color: <?php echo $mo_options->theme_options['primary_dark']; ?>;
						font-weight: 300;
					}
				<?php break; ?>
				<?php case 'medium': ?>
					.button {
						color: <?php echo $mo_options->theme_options['primary_dark']; ?>; 
						border-color: <?php echo $mo_options->theme_options['primary_dark']; ?>;
						border-width: 4px;
						font-weight: 500;
					}
				<?php break; ?>	
				<?php case 'bold': ?>
					.button {
						color: #fff; 
						border: none !important;
						background-color: <?php echo $mo_options->theme_options['primary_dark']; ?>;
						font-weight: 500;
					}
				<?php break; ?>		
							
			<?php } ?>			
			
	    <?php
		    die();
	}
	
	
	add_filter("mce_external_plugins", 'enqueue_plugin_scripts');
	add_filter("mce_buttons", 'register_buttons_editor');
	
	
	function enqueue_plugin_scripts($plugin_array) {
		
	    //enqueue TinyMCE plugin script with its ID.
	    $plugin_array["modern_button_plugin"] =  get_template_directory_uri() . "/inc/js/tinymce.js";
	    return $plugin_array;
	    
	}

	function register_buttons_editor($buttons) {
	    //register buttons with their id.
	    array_push($buttons, 'separator', "modern_button");
	    return $buttons;
	}

	// make the visual editor better
	function make_mce_awesome($init) {

		global $mo_options;
		
		// add custom fonts
		$theme_advanced_fonts = null;
		
		$theme_advanced_fonts .= 'Dancing Script=Dancing Script;';
		$theme_advanced_fonts .= 'Dancing Script Bold=Dancing Script Bold;';
		$theme_advanced_fonts .= 'Droid Sans=Droid Sans;';
		$theme_advanced_fonts .= 'Droid Sans Bold=Droid Sans Bold;';
		$theme_advanced_fonts .= 'Droid Serif=Droid Serif;';
		$theme_advanced_fonts .= 'Droid Serif Bold=Droid Serif Bold;';
		$theme_advanced_fonts .= 'Gilda Display=Gilda Display;';
		$theme_advanced_fonts .= 'Josefin Slab Light=Josefin Slab Light;';
		$theme_advanced_fonts .= 'Josefin Slab=Josefin Slab;';
		$theme_advanced_fonts .= 'Josefin Slab Bold=Josefin Slab Bold;';
		$theme_advanced_fonts .= 'Lato Light=Lato Light;';
		$theme_advanced_fonts .= 'Lato=Lato;';
		$theme_advanced_fonts .= 'Lato Bold=Lato Bold;';
		$theme_advanced_fonts .= 'Maven Pro=Maven Pro;';
		$theme_advanced_fonts .= 'Maven Pro Bold=Maven Pro Bold;';
		$theme_advanced_fonts .= 'Muli Light=Muli Light;';
		$theme_advanced_fonts .= 'Muli=Muli;';
		$theme_advanced_fonts .= 'Open Sans Light=Open Sans Light;';
		$theme_advanced_fonts .= 'Open Sans=Open Sans;';
		$theme_advanced_fonts .= 'Open Sans Bold=Open Sans Bold;';
		$theme_advanced_fonts .= 'Oswald Light=Oswald Light;';
		$theme_advanced_fonts .= 'Oswald=Oswald;';
		$theme_advanced_fonts .= 'Oswald Bold=Oswald Bold;';
		$theme_advanced_fonts .= 'Pacifico=Pacifico;';
		$theme_advanced_fonts .= 'Playfair Display=Playfair Display;';
		$theme_advanced_fonts .= 'Playfair Display Bold=Playfair Display Bold;';
		$theme_advanced_fonts .= 'Poiret One=Poiret One;';
		$theme_advanced_fonts .= 'Questrial=Questrial;';
		$theme_advanced_fonts .= 'Roboto Light=Roboto Light;';
		$theme_advanced_fonts .= 'Roboto=Roboto;';
		$theme_advanced_fonts .= 'Roboto Bold=Roboto Bold;';

		// Default fonts for TinyMCE
		$theme_advanced_fonts .= 'Andale Mono=Andale Mono, Times;';
		$theme_advanced_fonts .= 'Arial=Arial, Helvetica, sans-serif;';
		$theme_advanced_fonts .= 'Arial Black=Arial Black, Avant Garde;';
		$theme_advanced_fonts .= 'Book Antiqua=Book Antiqua, Palatino;';
		$theme_advanced_fonts .= 'Comic Sans MS=Comic Sans MS, sans-serif;';
		$theme_advanced_fonts .= 'Courier New=Courier New, Courier;';
		$theme_advanced_fonts .= 'Georgia=Georgia, Palatino;';
		$theme_advanced_fonts .= 'Helvetica=Helvetica;';
		$theme_advanced_fonts .= 'Impact=Impact, Chicago;';
		$theme_advanced_fonts .= 'Symbol=Symbol;';
		$theme_advanced_fonts .= 'Tahoma=Tahoma, Arial, Helvetica, sans-serif;';
		$theme_advanced_fonts .= 'Terminal=Terminal, Monaco;';
		$theme_advanced_fonts .= 'Times New Roman=Times New Roman, Times;';
		$theme_advanced_fonts .= 'Trebuchet MS=Trebuchet MS, Geneva;';
		$theme_advanced_fonts .= 'Verdana=Verdana, Geneva;';
		$theme_advanced_fonts .= 'Webdings=Webdings;';
		$theme_advanced_fonts .= 'Wingdings=Wingdings, Zapf Dingbats';

		// set font options
		if($theme_advanced_fonts){
			$init['font_formats'] = $theme_advanced_fonts;
		}
		return $init;
	}
	add_filter('tiny_mce_before_init', 'make_mce_awesome');

	// add font-family dropdown
	function tinymce_add_buttons($buttons) {
	     $buttons[] = 'fontselect';
	     $buttons[] = 'fontsizeselect';
	     return $buttons;
    }

    add_filter('mce_buttons', 'tinymce_add_buttons');

?>