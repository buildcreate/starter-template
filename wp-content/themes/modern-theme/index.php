<?php get_header(); ?>
	
	
	
	<?php do_action('mo_render_header', $mo_options); ?>
		
	<!-- section -->
	<section id="main" class="blog index" role="main">
		<div class="wrapper">
			<div class="content-wrap has-sidebar">
				<?php if (have_posts()): ?>
					<?php while (have_posts()) : the_post(); ?>
				
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>							
							<?php get_template_part('loop', 'index'); ?>				
					</article>
					<!-- /article -->
					
					<?php endwhile; ?>
					
					<?php get_template_part('pagination'); ?>
					
				<?php else: ?>
				
					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h2>
					</article>
					<!-- /article -->
							
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</section>
	<!-- /section -->
	
<?php get_footer(); ?>