<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<div class="post-meta">
	<span class="date"><?php  the_date(); ?></span>
	<span class="cats">Posted in: <?php the_category(', ') ?></span>
</div>
<div class="post-content">
	<?php the_excerpt(); ?>
</div>