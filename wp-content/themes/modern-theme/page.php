<?php get_header(); ?>
	
	
	
	<?php do_action('mo_render_header', $mo_options); ?>
		
	<!-- section -->
	<section id="main" role="main">
				
				<?php if (have_posts()): ?>
					<?php while (have_posts()) : the_post(); ?>
				
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
					</article>
					<!-- /article -->
					
					<?php endwhile; ?>
				<?php else: ?>
				
					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h2>
					</article>
					<!-- /article -->
				
				<?php endif; ?>
			
	</section>
	<!-- /section -->
	
<?php get_footer(); ?>