<?php get_header(); ?>
	<?php do_action('mo_render_header', $mo_options); ?>
	<!-- section -->
	<section id="main" role="main">
		<div class="wrapper">
	
		<div class="page-content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<?php if(has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<div class="staff-image">
						<?php the_post_thumbnail('large', array('class' => 'single-staff-image')); // Fullsize image for the single post ?>
					</div>
				<?php endif; ?>
				
				<div class="staff-info">

<!--
					<?php if($email = get_field('email')) : ?>
						<h4 class="email"><strong>Email:</strong> <?php echo $email; ?></h4>
					<?php endif; ?>
-->
					
					<?php the_content(); ?>	
				
				</div>
				
				
				<span class="clearer"></span>
			</article>
			<!-- /article -->
			
		<?php endwhile; ?>
		
		<?php else: ?>
		
			<!-- article -->
			<article>
				
				<h1><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h1>
				
			</article>
			<!-- /article -->
		
		<?php endif; ?>
		</div>
		<span class="clearer"></span>
		</div>
	</section>
	<!-- /section -->
	
<?php get_footer(); ?>