<?php get_header(); ?>
	
	<?php do_action('mo_render_header', $mo_options); ?>
	
	<!-- section -->
	<section id="main" role="main">
	
		<div class="wrapper">
			<div class="page-content">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<aside class="contact-info">
						<?php if(has_post_thumbnail()) : ?>
							<?php the_post_thumbnail('medium'); ?>
						<?php else : ?>
							<img src="<?php bloginfo('template_directory'); ?>/images/default-no-image.jpg" alt="" />
						<?php endif; ?>	
						<span class="clearer"></span>
						<div class="info">
							<?php if(get_field('email_contact')) : ?>
								<?php $emails = get_field('email_contact'); ?>
								Contact(s)
								<ul class="contacts">
								<?php foreach($emails as $email) : ?>
									<li>
										<p><a href="<?php echo bloginfo('url'); ?>/contact-us/?email_recipient=<?php echo $email['email']; ?>"><?php echo $email['name']; ?></a></p>
									</li>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>
							<?php if(get_field('website') || get_field('facebook') || get_field('twitter')) : ?>
								For more information
								<ul class="contacts">
									<?php if(get_field('website')) : ?>
										<li><a target="_blank" href="<?php the_field('website');?>">Website</a></li>
									<?php endif; ?>
									<?php if(get_field('facebook')) : ?>
										<li><a target="_blank" href="<?php the_field('facebook');?>">Facebook</a></li>
									<?php endif; ?>
									<?php if(get_field('twitter')) : ?>
										<li><a target="_blank" href="<?php the_field('twitter');?>">Twitter</a></li>
									<?php endif; ?>
								</ul>
							<?php endif; ?>
						</div>
					</aside>
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" class="student-group-content">
						
						<?php the_content(); ?>
						<?php //edit_post_link(); ?>
						<?php //comments_template(); ?>
						
					</article>
					<!-- /article -->
					
				<?php endwhile; ?>
				
				<?php else: ?>
				
					<!-- article -->
					<article>
						
						<h1><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h1>
						
					</article>
					<!-- /article -->
				
				<?php endif; ?>
				
				
			</div>
			<?php get_sidebar(); ?>
			<span class="clearer"></span>
		</div>
	</section>
	<!-- /section -->
	
<?php get_footer(); ?>