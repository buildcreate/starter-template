<?php get_header(); ?>
	
<?php do_action('mo_render_header', $mo_options); ?>
	
	<!-- section -->
	<section id="main" class="blog single" role="main">
		
		<div class="wrapper">
			<div class="content-wrap has-sidebar">
				<div class="page-content">
					
					<!-- 	<h1 class="page-title"><?php the_title(); ?></h1> -->
					
					<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					
						<!-- article -->
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							
							<?php the_content(); ?>

							<?php //edit_post_link(); ?>
							<?php //comments_template(); ?>
							
						</article>
						<!-- /article -->
						
						<div class="pagination">	
							<div class="alignleft"><?php previous_post_link(); ?></div> 
							<div class="alignright"><?php next_post_link(); ?></div>
						</div>
						
					<?php endwhile; ?>
					
					<?php else: ?>
					
						<!-- article -->
						<article>
							
							<h1><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h1>
							
						</article>
						<!-- /article -->
					
					<?php endif; ?>
					
					
				</div>
				
			</div>
			<?php get_sidebar(); ?>
		</div>
	</section>
	<!-- /section -->
	
<?php get_footer(); ?>