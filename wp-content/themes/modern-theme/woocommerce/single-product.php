<?php get_header(); ?>
	
	<?php do_action('mo_render_header', $mo_options); ?>
	
	<!-- section -->
	<section id="main" role="main">
		<div class="content-row">
			<div class="wrapper" style="max-width: <?php echo $mo_options->site_options['site_max_width']?>">

				<?php
				/**
				 * The Template for displaying all single products.
				 *
				 * Override this template by copying it to yourtheme/woocommerce/single-product.php
				 *
				 * @author 		WooThemes
				 * @package 	WooCommerce/Templates
				 * @version     1.6.4
				 */

				if ( ! defined( 'ABSPATH' ) ) {
					exit; // Exit if accessed directly
				}

				//get_header( 'shop' ); ?>

					<?php
						/**
						 * woocommerce_before_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
					?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php wc_get_template_part( 'content', 'single-product' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php
						/**
						 * woocommerce_after_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						do_action( 'woocommerce_after_main_content' );
					?>

					<?php
						/**
						 * woocommerce_sidebar hook
						 *
						 * @hooked woocommerce_get_sidebar - 10
						 */
						//do_action( 'woocommerce_sidebar' );
					?>

				<?php //get_footer( 'shop' ); ?>


			<?php //get_sidebar(); ?>
			<span class="clearer"></span>
			</div>
		</div>
	</section>
	<!-- /section -->
	
<?php get_footer(); ?>

